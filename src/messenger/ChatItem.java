package messenger;


public class ChatItem {
    private final String message;
    private final int senderID;//0=esta máquina , 1=o contato
    public ChatItem(String message, int id){
        this.message=message;
        this.senderID=id;
    }
    public String getMessage(){
        return this.message;
    }
    public int getSender(){
        return this.senderID;
    }
}
