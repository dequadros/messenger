/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messenger;

import java.net.InetAddress;
import java.util.ArrayList;

/**
 *
 * @author giovani
 */
public class Contact {
    private final String peerName;
    private final InetAddress peerIP;
    private final ArrayList<ChatItem> chatHistory;
    
    public String getName(){
        return peerName;
    }
    public Contact(String name, InetAddress ip){
        
        this.peerName=name;
        this.peerIP=ip;
        this.chatHistory=new ArrayList<>();
    }
    
    public void addMessage(String msg,int id){
        //Someone said something
        chatHistory.add(new ChatItem(msg,id));
    }
    public String getChatText(){
        int lastPerson=-1;
        //histórico do texto que aparecerá na tela
        String chatText="";
        ChatItem current;
        for (ChatItem chatHistory1 : chatHistory) {
            current = chatHistory1;
            switch(current.getSender()){//verifica quem enviou a mensagem
                case 0:
                    if (lastPerson!=0){
                        chatText+="You say:\n";
                        lastPerson=0;
                    }
                    break;
                case 1:
                    if(lastPerson!=1){
                        chatText+=peerName+" says:\n";
                        lastPerson=1;
                    }
                    break;
            }
            chatText+="   "+current.getMessage()+"\n";
        }
        return chatText;
    }
}
