/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messenger;

import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author giovani
 */
public class Messenger {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws URISyntaxException {
        // TODO code application logic here
       
        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Messenger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(Messenger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(Messenger.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Messenger.class.getName()).log(Level.SEVERE, null, ex);
        }
         //MainWindow main= new MainWindow();
        //main.setVisible(true);
        LoginWindow loginWindow=new LoginWindow();
        loginWindow.setResizable(false);
        loginWindow.setVisible(true);
        playSound("startup.wav");
    }
    public static synchronized void playSound(final String url) {
            new Thread(new Runnable() {
            // The wrapper thread is unnecessary, unless it blocks on the
            // Clip finishing; see comments.
              public void run() {
                try {
                  Clip clip = AudioSystem.getClip();
                  AudioInputStream inputStream = AudioSystem.getAudioInputStream(
                    Messenger.class.getResourceAsStream(url));
                  clip.open(inputStream);
                  clip.start(); 
                } catch (Exception e) {
                  System.err.println(e.getMessage());
                }
              }
            }).start();
}
}
